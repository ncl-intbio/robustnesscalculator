package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import checker.STLChecker;
import data.Trace;

public class Main {

	public static void main(String[] args) {
		try {
			if (args.length > 1) {
				String property = args[1];
				for (int i = 2; i < args.length; i++) {
					property += args[i];
				}
				System.out.println(getRobustness(args[0], property));
				// System.out.println(check.robustness("G[0,10000](der(GFP_rrnb)=4)"));
				// System.out.println(check.robustness("F[0,1000](G[0,10](abs(der(GFP_rrnb))<0.00000001))"));
			}
			else {
				System.out.println("Incorrect number of arguments.");
				System.out.println("Usage:  java -jar RobustnessCalculator-<version>.jar <copasi_data_file> <property_to_check>");
			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		List<Trace> traces;
//		STLChecker check;
//		try {
//			if (args.length > 1) {
//				traces = parseCopasiParameterScanOutput(new File(args[0]));
//				String property = args[1];
//				for (int i = 2; i < args.length; i++) {
//					property += args[i];
//				}
//				double[] timePoints = new double[traces.size()];
//				double[][] data = new double[traces.size()][];
//				for (int i = 0; i < traces.size(); i++) {
//					timePoints[i] = traces
//							.get(i)
//							.getValue(
//									"Subtilin",
//									traces.get(i).getTimePoints()[traces.get(i).getTimePoints().length - 1]);
//					data[i] = new double[] { traces
//							.get(i)
//							.getValue(
//									"GFP_rrnb",
//									traces.get(i).getTimePoints()[traces.get(i).getTimePoints().length - 1]) };
//				}
//				Trace trace = new Trace(new String[] { "GFP_rrnb" }, timePoints, data);
//				// System.out.println(trace);
//				check = new STLChecker(trace);
//				System.out.println(check.robustness(property));
//			}
//			else {
//				System.out.println("Incorrect number of arguments.");
//				System.out.println("Usage:  java Main <copasi_data_file> <property_to_check>");
//			}
//		}
//		catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		// List<String> vars = new ArrayList<String>();
		// vars.add("a");
		// vars.add("b");
		// List<Double> timePoints = new ArrayList<Double>();
		// timePoints.add(0.0);
		// timePoints.add(1.0);
		// timePoints.add(2.0);
		// timePoints.add(3.0);
		// timePoints.add(4.0);
		// // timePoints.add(5.0);
		// // timePoints.add(6.0);
		// // timePoints.add(7.0);
		// // timePoints.add(8.0);
		// // timePoints.add(9.0);
		// // timePoints.add(10.0);
		// List<Double> aVals = new ArrayList<Double>();
		// aVals.add(0.0);
		// aVals.add(9.0);
		// aVals.add(9.0);
		// aVals.add(9.0);
		// aVals.add(9.0);
		// // aVals.add(6.0);
		// // aVals.add(7.0);
		// // aVals.add(5.0);
		// // aVals.add(3.0);
		// // aVals.add(2.0);
		// // aVals.add(1.0);
		// List<Double> bVals = new ArrayList<Double>();
		// bVals.add(0.0);
		// bVals.add(3.0);
		// bVals.add(2.0);
		// bVals.add(5.0);
		// bVals.add(6.0);
		// // bVals.add(6.0);
		// // bVals.add(8.0);
		// // bVals.add(9.0);
		// // bVals.add(7.0);
		// // bVals.add(6.0);
		// // bVals.add(9.0);
		// List<List<Double>> vals = new ArrayList<List<Double>>();
		// vals.add(aVals);
		// vals.add(bVals);
		// Trace trace = new Trace(vars, timePoints, vals);
		// STLChecker check = new STLChecker(trace);
		// System.out.println(check.robustness("(a>=7)U[1,3](b>=4)"));

		// List<String> vars = new ArrayList<String>();
		// vars.add("a");
		// List<Double> timePoints = new ArrayList<Double>();
		// timePoints.add(0.0);
		// timePoints.add(1.0);
		// timePoints.add(2.0);
		// timePoints.add(3.0);
		// timePoints.add(4.0);
		// timePoints.add(5.0);
		// timePoints.add(6.0);
		// List<Double> aVals = new ArrayList<Double>();
		// aVals.add(5.0);
		// aVals.add(7.0);
		// aVals.add(2.0);
		// aVals.add(9.0);
		// aVals.add(3.0);
		// aVals.add(4.0);
		// aVals.add(8.0);
		// List<List<Double>> vals = new ArrayList<List<Double>>();
		// vals.add(aVals);
		// Trace trace = new Trace(vars, timePoints, vals);
		// STLChecker check = new STLChecker("F[1,3](a>0)", trace);
		// System.out.println(check.robustness());
	}
	
	public static double getRobustness(String trace, String property) throws IOException {
		return getRobustness(parseCopasiOutput(new File(trace)), property);
	}
	
	public static double getRobustness(Trace trace, String property) {
		STLChecker check = new STLChecker(trace);
		return check.robustness(property);
	}

	public static List<Trace> parseCopasiParameterScanOutput(File file) throws IOException {
		List<Trace> traces = new ArrayList<Trace>();
		Scanner scanner = new Scanner(file);
		List<String> variables = new ArrayList<String>();
		if (scanner.hasNextLine()) {
			String[] vars = scanner.nextLine().split("\t");
			for (int i = 2; i < vars.length; i++) {
				if (vars[i].startsWith("[") && vars[i].endsWith("]")) {
					variables.add(vars[i].substring(1, vars[i].length() - 1));
				}
				else {
					variables.add(vars[i]);
				}
			}
		}
		while (scanner.hasNextLine()) {
			List<Double> timePoints = new ArrayList<Double>();
			List<List<Double>> data = new ArrayList<List<Double>>();
			for (int i = 0; i < variables.size(); i++) {
				data.add(new ArrayList<Double>());
			}
			while (scanner.hasNextLine()) {
				String dataLine = scanner.nextLine().trim();
				if (dataLine.equals("")) {
					break;
				}
				else {
					String[] line = dataLine.split("\t");
					timePoints.add(Double.parseDouble(line[1]));
					for (int i = 2; i < line.length; i++) {
						data.get(i - 2).add(Double.parseDouble(line[i]));
					}
				}
			}
			if (timePoints.size() > 0) {
				traces.add(new Trace(variables, timePoints, data));
			}
		}
		scanner.close();
		return traces;
	}

	public static Trace parseCopasiOutput(File file) throws IOException {
		Scanner scanner = new Scanner(file);
		List<String> variables = new ArrayList<String>();
		List<Double> timePoints = new ArrayList<Double>();
		List<List<Double>> data = new ArrayList<List<Double>>();
		if (scanner.hasNextLine()) {
			String[] vars = scanner.nextLine().split("\t");
			for (int i = 1; i < vars.length; i++) {
				variables.add(vars[i]);
			}
		}
		for (int i = 0; i < variables.size(); i++) {
			data.add(new ArrayList<Double>());
		}
		while (scanner.hasNextLine()) {
			String[] dataLine = scanner.nextLine().split("\t");
			timePoints.add(Double.parseDouble(dataLine[0]));
			for (int i = 1; i < dataLine.length; i++) {
				data.get(i - 1).add(Double.parseDouble(dataLine[i]));
			}
		}
		scanner.close();
		return new Trace(variables, timePoints, data);
	}
}
